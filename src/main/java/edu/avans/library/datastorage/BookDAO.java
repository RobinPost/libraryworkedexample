package edu.avans.library.datastorage;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import edu.avans.library.domain.Book;

public class BookDAO {
	
	public BookDAO() {
		
	}
	
	public Book findBookByISBN(int isbn) {
		Book book = null;
		
		DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultSet = connection.executeSQLSelectStatement(
                    "SELECT * FROM book WHERE ISBN = " + isbn + ";");
            
            if (resultSet != null) {
            	try {
            		if (resultSet.next()) {
            			int isbnFromDb = resultSet.getInt("ISBN");
						String title = resultSet.getString("Title");
						String author = resultSet.getString("Author");
						int edition = resultSet.getInt("Edition");
						
						book = new Book(isbnFromDb, title, author, edition);
						
            		}
            	} catch (Exception e) {
            		e.printStackTrace();
            	}
            	
            	connection.closeConnection();
            }
        }
		
		return book;
	}

}
