package edu.avans.library.presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class AddItemUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddItemUI frame = new AddItemUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddItemUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 395, 247);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblItemToevoegen = new JLabel("Item toevoegen");
		lblItemToevoegen.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		lblItemToevoegen.setBounds(10, 11, 144, 27);
		contentPane.add(lblItemToevoegen);
		
		JLabel lblIsbn = new JLabel("ISBN");
		lblIsbn.setBounds(20, 49, 46, 14);
		contentPane.add(lblIsbn);
		
		textField = new JTextField();
		textField.setBounds(68, 46, 187, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblTitel = new JLabel("Titel");
		lblTitel.setBounds(20, 74, 46, 14);
		contentPane.add(lblTitel);
		
		JLabel lblAuteur = new JLabel("Auteur");
		lblAuteur.setBounds(20, 99, 46, 14);
		contentPane.add(lblAuteur);
		
		JLabel lblEditie = new JLabel("Editie");
		lblEditie.setBounds(20, 124, 46, 14);
		contentPane.add(lblEditie);
		
		textField_1 = new JTextField();
		textField_1.setBounds(68, 71, 187, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(68, 96, 187, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(68, 121, 40, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnToevoegen = new JButton("Toevoegen");
		btnToevoegen.setBounds(280, 174, 89, 23);
		contentPane.add(btnToevoegen);
	}
}
