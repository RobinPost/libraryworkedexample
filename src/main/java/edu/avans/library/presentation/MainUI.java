package edu.avans.library.presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import edu.avans.library.businesslogic.MemberAdminManager;
import edu.avans.library.datastorage.BookDAO;
import edu.avans.library.domain.Book;
import edu.avans.library.domain.Member;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JList;
import javax.swing.JTextArea;
import java.util.List;

public class MainUI extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldMembershipNr;
	private JTextArea textAreaMemberInfo, textAreaBookInfo;
	
	private final MemberAdminManager manager;
	
	private Member currentMember;
	
	private BookDAO bookDAO = new BookDAO();
	private List<Book> books = new ArrayList<>();
	private JTextField textFieldISBN;

	public MainUI(MemberAdminManager memberAdminManager) {
		setUp();
		
		manager = memberAdminManager;
        currentMember = null;
        
	}
	
	private void setUp() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 465, 336);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLibrary = new JLabel("Library");
		lblLibrary.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		lblLibrary.setBounds(10, 11, 65, 27);
		contentPane.add(lblLibrary);
		
		JLabel lblZoekNaarLeden = new JLabel("Zoek naar leden");
		lblZoekNaarLeden.setBounds(10, 49, 202, 14);
		contentPane.add(lblZoekNaarLeden);
		
		textFieldMembershipNr = new JTextField();
		textFieldMembershipNr.setBounds(10, 74, 114, 20);
		contentPane.add(textFieldMembershipNr);
		textFieldMembershipNr.setColumns(10);
		
		JButton searchButton = new JButton("Zoeken");
		searchButton.setBounds(134, 73, 78, 23);
		contentPane.add(searchButton);
		
		JLabel lblOverzichtVanItems = new JLabel("Zoeken naar boeken");
		lblOverzichtVanItems.setBounds(243, 49, 185, 14);
		contentPane.add(lblOverzichtVanItems);
		
		JButton btnToevoegen = new JButton("Toevoegen");
		btnToevoegen.setBounds(187, 18, 97, 23);
		contentPane.add(btnToevoegen);
		
		textAreaMemberInfo = new JTextArea();
		textAreaMemberInfo.setEditable(false);
		textAreaMemberInfo.setBounds(10, 105, 200, 189);
		contentPane.add(textAreaMemberInfo);
		
		JButton btnWijzigen = new JButton("Wijzigen");
		btnWijzigen.setBounds(306, 18, 78, 23);
		contentPane.add(btnWijzigen);
		
		textAreaBookInfo = new JTextArea();
		textAreaBookInfo.setEditable(false);
		textAreaBookInfo.setBounds(243, 107, 200, 187);
		contentPane.add(textAreaBookInfo);
		
		textFieldISBN = new JTextField();
		textFieldISBN.setBounds(243, 74, 114, 20);
		contentPane.add(textFieldISBN);
		textFieldISBN.setColumns(10);
		
		JButton btnFindBook = new JButton("Zoeken");
		btnFindBook.setBounds(365, 73, 78, 23);
		contentPane.add(btnFindBook);
		
		searchButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int membershipNr = Integer.parseInt(textFieldMembershipNr.getText());
				doFindMember(membershipNr);
			}
		});
		
		btnFindBook.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int isbn = Integer.parseInt(textFieldISBN.getText());
				doFindBook(isbn);
			}
		});
		
		btnToevoegen.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AddItemUI addItemUI = new AddItemUI();
				addItemUI.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
				addItemUI.setVisible(true);
			}
		});
	}
	
	private void doFindMember(int membershipNr) {
        currentMember = manager.findMember(membershipNr);
        String memberInfo = "Lid niet gevonden";
        boolean memberCanBeRemoved = false;

        if (currentMember != null) {
            String boekenGeleend = "nee";
            if (currentMember.hasLoans()) {
                boekenGeleend = "ja";
            }

            String heeftReserveringen = "nee";
            if (currentMember.hasReservations()) {
                heeftReserveringen = "ja";
            }

            memberInfo
                    = currentMember.getFirstName() + " " + currentMember.getLastname()
                    + "\n"
                    + currentMember.getStreet() + " " + currentMember.getHouseNumber()
                    + "\n"
                    + currentMember.getCity()
                    + "\n"
                    + "\n"
                    + "Boete: " + currentMember.getFine()
                    + "\n"
                    + "\n"
                    + "Heeft boeken geleend: " + boekenGeleend
                    + "\n"
                    + "Heeft reserveringen: " + heeftReserveringen;

            memberCanBeRemoved = currentMember.isRemovable();
        }
        // else memberInfo has already proper value. The button that removes a
        // member from the system needs to be disabled. No work needed for that
        // in the else since the value of memberCanBeRemoved is correct.

//        removeMemberButton.setEnabled(memberCanBeRemoved);
        textAreaMemberInfo.setText(memberInfo);
    }
	
	private void doFindBook(int isbn) {
		Book book = bookDAO.findBookByISBN(isbn);
		String bookInfo = null;
		
		if (book != null) {
			bookInfo = 
					"Titel: " + book.getTitle().toString() + "\n" +
					"Auteur: " + book.getAuthor() + "\n" +
					"Editie: " + book.getEdition();
		}
		
		textAreaBookInfo.setText(bookInfo);
	}
}
